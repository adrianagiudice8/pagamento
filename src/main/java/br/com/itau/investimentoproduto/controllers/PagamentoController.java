package br.com.itau.investimentoproduto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimentoproduto.services.PagamentoService;

@RestController
@RequestMapping
public class PagamentoController {
	@Autowired
	PagamentoService pagamentoService;

	@GetMapping
	public boolean buscar() {
		boolean pagamento = pagamentoService.getRandomBoolean();
		return pagamento;
	}
}
