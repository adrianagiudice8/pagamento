package br.com.itau.investimentoproduto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimentoproduto.models.Pagamento;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

}
